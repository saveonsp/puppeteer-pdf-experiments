import * as d3 from "d3";
import * as prettyms from "pretty-ms";

const [width, height] = [800, 400];
const margin = { top: 30, right: 50, bottom: 40, left: 50 };

const xAxis = (g) =>
  g
    .attr("transform", `translate(0,${height - margin.bottom})`)
    .call(
      d3
        .axisBottom(x)
        .ticks(width / 80)
        .tickSizeOuter(0)
    )
    .call((g) =>
      g
        .append("text")
        .attr("x", margin.left + (width - margin.right) / 2)
        .attr("y", 30)
        .attr("fill", "currentColor")
        .attr("font-weight", "bold")
        .attr("text-anchor", "middle")
        .text("concurrent pages")
    );

const yAxisLeft = (g) =>
  g
    .attr("transform", `translate(${margin.left},0)`)
    .call(
      d3
        .axisLeft(y0)
        .tickFormat(prettyms)
        .ticks(height / 40)
    )
    .call((g) => g.select(".domain").remove())
    .call((g) =>
      g
        .select(".tick:last-of-type text")
        .clone()
        .attr("x", 4)
        .attr("text-anchor", "start")
        .attr("font-weight", "bold")
        .text("total")
    );

const yAxisRight = (g) =>
  g
    .attr("transform", `translate(${width - margin.right},0)`)
    .call(
      d3
        .axisRight(y1)
        .tickFormat(prettyms)
        .ticks(height / 40)
    )
    .call((g) => g.select(".domain").remove())
    .call((g) =>
      g
        .select(".tick:last-of-type text")
        .clone()
        .attr("x", -4)
        .attr("text-anchor", "end")
        .attr("font-weight", "bold")
        .text("average")
    );

const x = d3
  .scaleBand()
  .paddingInner(0.8)
  .paddingOuter(0.4)
  .range([margin.left, width - margin.right]);

const y0 = d3
  .scaleLinear()
  .nice()
  .range([height - margin.bottom, margin.top]);
const y1 = d3
  .scaleLinear()
  .nice()
  .range([height - margin.bottom, margin.top]);

(async () => {
  const data = await fetch("/data.csv")
    .then((r) => r.text())
    .then((text) =>
      d3.csvParse(text, (r) =>
        Object.fromEntries(Object.entries(r).map(([k, v]) => [k, +v]))
      )
    );

  const range = Object.fromEntries(
    Object.keys(data[0]).map((k) => [k, d3.extent(data, (d) => d[k])])
  );

  x.domain(data.map((d) => d.concurrency));
  y0.domain([0, range.total[1]]);
  y1.domain([0, range.average[1]]);

  const svg = d3
    .select(document.body)
    .append("svg")
    .attr("xmlns", "http://www.w3.org/2000/svg")
    .attr("viewBox", [0, 0, width, height]);
  const bandwidth = x.bandwidth();
  {
    const color = "blue";
    const line = d3
      .line()
      .curve(d3.curveStep)
      .x((d) => x(d.concurrency))
      .y((d) => y0(d.total));
    svg.append("g").attr("color", color).call(yAxisLeft);
    svg
      .append("g")
      .attr("fill", color)
      .selectAll("rect")
      .data(data)
      .join("rect")
      .attr("width", bandwidth)
      .attr("height", (d) => -y0(d.total) + y0(0))
      .attr("y", (d) => y0(d.total))
      .attr("x", (d) => x(d.concurrency) - bandwidth / 2);
  }
  {
    const color = "red";
    const line = d3
      .line()
      .curve(d3.curveStep)
      .x((d) => x(d.concurrency))
      .y((d) => y1(d.average));
    svg.append("g").attr("color", color).call(yAxisRight);
    svg
      .append("g")
      .attr("fill", color)
      .selectAll("rect")
      .data(data)
      .join("rect")
      .attr("width", bandwidth)
      .attr("height", (d) => -y1(d.average) + y1(0))
      .attr("y", (d) => y1(d.average))
      .attr("x", (d) => x(d.concurrency) + bandwidth / 2);
  }
  svg.append("g").call(xAxis);

  const serializer = new XMLSerializer();
  let source = serializer.serializeToString(svg.node());

  //add name spaces.
  // if (!source.match(/^<svg[^>]+xmlns="http\:\/\/www\.w3\.org\/2000\/svg"/)) {
  //   source = source.replace(/^<svg/, '<svg xmlns="http://www.w3.org/2000/svg"');
  // }
  // if (!source.match(/^<svg[^>]+"http\:\/\/www\.w3\.org\/1999\/xlink"/)) {
  //   source = source.replace(
  //     /^<svg/,
  //     '<svg xmlns:xlink="http://www.w3.org/1999/xlink"'
  //   );
  // }

  //add xml declaration
  source = `<?xml version="1.0" standalone="no"?>\r\n${source}`;

  //convert svg source to URI data scheme.
  const url = `data:image/svg+xml;charset=utf-8,${encodeURIComponent(source)}`;

  d3.select(document.body)
    .selectAll("a")
    .data([null])
    .join("a")
    .attr("href", url)
    .attr("download", "graph.svg")
    .text("download");
})();

const path = require("path");

module.exports = {
  mode: 'development',
  entry: path.join(__dirname, "./graph.ts"),
  module: {
    rules: [
      {
        test: /\.ts?$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: [".ts", ".js"],
  },
  output: {
    filename: "graph.js",
    path: __dirname,
  },
};

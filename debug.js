const main = require("./main");

(async () => {
  await main.createServer(3000);
})();

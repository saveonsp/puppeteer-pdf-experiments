const fs = require("fs");
const { promises: fsp } = fs;
const http = require("http");
const path = require("path");
const {
  Readable,
  promises: { pipeline },
} = require("stream");

const express = require("express");
const puppeteer = require("puppeteer");
const prettyms = require("pretty-ms");
const csv = require("csv");

const NS_PER_SEC = 1e9;

// create readablestream
//   from query
//   from csv via http
//
// pipe through request transformer (tableau)
// pipe through pdf renderer
// pipe through pdf merger
//
// pipe to directory OR
// pipe to zip file and stream to client

async function main() {
  const port = 3000;

  const cleanup = await createServer(port);

  const data = [];
  const length = 10;
  for (let concurrency = 1; concurrency < 11; concurrency++) {
    console.log(`Concurrency ${concurrency}`);

    // await pageMethod(concurrency)(async (args) => {
    await browserMethod(concurrency)(async (args) => {
      let results;
      const total = await timer(async () => {
        const trials = Array.from(Array(length)).map((_, i) =>
          timer.bind(null, async (page) => {
            const origin = `http://localhost:${port}`;
            await page.goto(`${origin}/csv`, { waitUntil: "domcontentloaded" });
            const buf = await page.pdf();
            await fsp.writeFile(`pages_${i}.pdf`, buf);
          })
        );
        results = await Processor.create(trials, args);
      });
      console.log(results);
      const average = results.reduce((a, b) => a + b[0], 0) / results.length;
      data.push({ concurrency, average, total });
    });
  }

  await pipeline(
    Readable.from(data),
    csv.stringify({ header: true }),
    fs.createWriteStream("data.csv")
  );

  await cleanup();
}

class Processor {
  constructor(input, resources) {
    this.result = [];
    this.index = -1;
    this.running = 0;
    this.resources = resources;
    this.max = resources.length;
    this.input = input;
    this.length = input.length;
    this.result = Array(this.length).fill(null);
  }

  check() {
    while (this.index < this.length - 1 && this.resources.length > 0) {
      const i = ++this.index;
      const [fn] = this.input.splice(i, 1, null);
      const args = this.resources.shift();
      this.running++;
      ((_i, _args) =>
        fn
          .apply(null, _args)
          .then(
            (...results) => (this.result[_i] = results),
            (err) => (this.result[_i] = err)
          )
          .then(() => {
            this.running--;
            this.resources.push(_args);
            this.check();
          }))(i, args);
    }
    if (this.running == 0 && this.index == this.result.length - 1) {
      this.resolve(this.result);
    }
  }

  run() {
    return new Promise((resolve, reject) => {
      this.resolve = resolve;
      this.reject = reject;
      this.check();
    });
  }

  static create(arr, max) {
    const p = new this(arr, max);
    return p.run();
  }
}

function pageMethod(scale) {
  return async (fn) => {
    const browser = await puppeteer.launch();
    const pages = await Promise.all(
      Array.from(Array(scale)).map(() => browser.newPage())
    );
    await fn(pages.map((p) => [p]));
    await browser.close();
  };
}

function browserMethod(scale) {
  return async (fn) => {
    const browsers = await Promise.all(
      Array.from(Array(scale)).map(() => puppeteer.launch())
    );
    const pages = await Promise.all(
      browsers.map((browser) => browser.newPage())
    );
    await fn(pages.map((p) => [p]));
    await Promise.all(browsers.map((browser) => browser.close()));
  };
}

async function timer(fn, ...args) {
  const start = process.hrtime();
  await fn.apply(this, args);
  const diff = process.hrtime(start);
  return (diff[0] * NS_PER_SEC + diff[1]) / 1e6;
}

function createServer(port = 3000) {
  const app = express();
  app.set("view engine", "pug");
  app.set("views", __dirname);

  app.get("/", (req, res) => {
    res.render("index");
  });

  app.get("/graph", (req, res) => {
    res.render("graph");
  });

  app.get("/csv", (req, res) => {
    const stream = csv
      .generate({
        objectMode: true,
        seed: 1,
        headers: 2,
        length: 2,
      })
      .pipe(
        csv.stringify({
          header: true,
          columns: {
            year: "birthYear",
            phone: "phone",
          },
        })
      );

    stream.pipe(res);
  });

  app.use(express.static(__dirname));

  const server = http.createServer(app);

  const cleanup = async () => {
    await new Promise((r) => server.close(r));
  };

  return new Promise((resolve, reject) => {
    server
      .listen(port)
      .once("listening", () => resolve(cleanup))
      .once("error", reject);
  });
}

module.exports = { createServer };

if (require.main === module) {
  main();
}
